#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include "configure.h"
using namespace std;

class core_desc{
public:
	int core_number[2];
	static void * processing(void *threadid){
		return NULL;
	}
	static void * router(void *threadid){
		return NULL;
	}
	int east[2];
	int west[2];
	int north[2];
	int south[2];
	int init(int m,int n){
		core_number[0]=m;
		core_number[1]=n;
		return 0;
	}
};
core_desc core[topology_m][topology_n];
pthread_t core_threads[topology_m*topology_n];
pthread_t flit_thread;
pthread_t router_threads[topology_m*topology_n];
void core_init(){
	int rc;
	int *core_thread_no = (int *) malloc(sizeof(*core_thread_no));
	*core_thread_no = 1;
	for(int i=0; i < topology_m;i++){
		for(int j=0; j < topology_n;j++){
			core[i][j].init(i,j);
			rc = pthread_create(&core_threads[i], NULL, core[i][j].processing, (void *)core_thread_no);
			core_thread_no++;
			if(rc){
			printf("can't create core thread\n");
			exit(-1);
		}
		}
	}
	for(int i=0;i<topology_m*topology_n;i++){
		
	}
}

void router_init(){
	int rc;
	int *router_thread_no = (int *) malloc(sizeof(*router_thread_no));
	*router_thread_no = 1;
	for(int i=0; i < topology_m;i++){
		for(int j=0; j < topology_n;j++){
			rc = pthread_create(&router_threads[i], NULL, core[i][j].router, (void *)router_thread_no);
			router_thread_no++;
			if(rc){
			printf("can't create thread\n");
			exit(-1);
		}
		}
	}
	for(int i=0;i<topology_m*topology_n;i++){
		
	}
}
void network_init(){
	for(int i=0;i<topology_m;i++){
		for(int j=0;j<topology_n;j++){
			core[i][j].east[0] = i;
			core[i][j].east[1] = j + 1;
			core[i][j].west[0] = i;
			core[i][j].west[1] = j-1;
			core[i][j].south[0] = i+1;
			core[i][j].south[1] = j;
			core[i][j].north[0] = i-1;
			core[i][j].north[1] = j;

			if(i==0){
				core[i][j].north[0] = 999;
				core[i][j].north[1] = 999;
			}
			if(j==0){
				core[i][j].west[0] = 9999;
				core[i][j].west[1] = 999;
			}
			if(i==(topology_m-1)){
				core[i][j].south[0] = 9999;
				core[i][j].south[1] = 9999;
			}
			if(j==(topology_n-1)){
				core[i][j].east[0] = 9999;
				core[i][j].east[1] = 9999;
			}	
		}
	}
}
void print_topology(){
		FILE *fp;
		fp = fopen("networks_architecture.txt","w+");
		fprintf(fp,"**************-----Network architecture-----**************\n\n");
		int i,j;
		for(i=0; i < topology_m;i++){
			for(j=0; j < topology_n;j++){
				if(core[i][j].core_number[1]==topology_n-1){
					fprintf(fp,"[%d][%d]",core[i][j].core_number[0],core[i][j].core_number[1]);
				}
				else
					fprintf(fp,"[%d][%d]---------",core[i][j].core_number[0],core[i][j].core_number[1]);
			}
			fprintf(fp,"\n");
			if(i != topology_m-1){
				fprintf(fp,"  ");
				for(int k=0;k<topology_n;k++){
					fprintf(fp,"|              ");
				}
				fprintf(fp,"\n");
				fprintf(fp,"  ");
				for(int k=0;k<topology_n;k++){
					fprintf(fp,"|              ");
				}
				fprintf(fp,"\n");
				fprintf(fp,"  ");
				for(int k=0;k<topology_n;k++){
					fprintf(fp,"|              ");
				}
				fprintf(fp,"\n");
				fprintf(fp,"  ");
				for(int k=0;k<topology_n;k++){
					fprintf(fp,"|              ");
				}
			}
			fprintf(fp,"\n");
		}
}
void print_router_links(){
	FILE *fp;
	fp = fopen("network_links.txt","w+");
	fprintf(fp,"**************-----Network links-----**************\n\n");
	fprintf(fp,"-----------------------------------------------------------------------\n");
	int i,j;
	for(i=0; i < topology_m;i++){
			for(j=0; j < topology_n;j++){
					fprintf(fp,"core : [%d][%d]\n",core[i][j].core_number[0],core[i][j].core_number[1]);
					fprintf(fp,"-----------------------------------------------------------------------\n");
					fprintf(fp,"\teast [%d,%d] \n \twest [%d,%d] \n \tnorth [%d,%d] \n \tsouth [%d,%d]\n",
						core[i][j].east[0],core[i][j].east[1],core[i][j].west[0],core[i][j].west[1],
						core[i][j].north[0],core[i][j].north[1],core[i][j].south[0],core[i][j].south[1]);
					fprintf(fp,"-----------------------------------------------------------------------\n");
		}			
	}
}
void *flit_generator(void* flit_no){
	for(int i=0;i<number_of_flits;i++){
		printf("%d\n",i);
	}
	return NULL;
}
void flit_generator_initializer(){
	int rc;
	int *flit_thread_no = (int *) malloc(sizeof(*flit_thread_no));
	*flit_thread_no = 1;
	rc = pthread_create(&flit_thread, NULL, flit_generator, (void *)flit_thread_no);
	if(rc){
		printf("Can't create flit generation thread\n");
		exit(-11);
	}
}
int main(){
	if(topology_n*topology_m > 100){
		printf("Silulation is limited to total of hundred cores, make changes in configure.h\n");
	}
	core_init();
	router_init();
	network_init();
	flit_generator_initializer();
	print_topology();
	print_router_links();
	return 0;
}